package br.com.api.digivox.api.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Item extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	
	@Column(nullable = false)
	@NotBlank
	private String nome;
	
	@Column(unique = true, nullable = false)
	private long codigo;
	
	@ManyToOne(optional = false)
	private TipoItem tipo;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "itens")
	private List<Reserva> reservas;
	
	@JsonIgnore
	@ManyToMany(mappedBy = "itens")
	private List<Aluguel> alugueis;

	
	public TipoItem getTipo() {
		return tipo;
	}

	public void setTipo(TipoItem tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}

	public List<Aluguel> getAlugueis() {
		return alugueis;
	}

	public void setAlugueis(List<Aluguel> alugueis) {
		this.alugueis = alugueis;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
}
