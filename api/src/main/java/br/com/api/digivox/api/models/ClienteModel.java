package br.com.api.digivox.api.models;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.api.digivox.api.entity.Aluguel;
import br.com.api.digivox.api.entity.Cliente;
import br.com.api.digivox.api.entity.Reserva;
import br.com.api.digivox.api.error.ApiException;
import br.com.api.digivox.api.error.DuplicatedKeyException;
import br.com.api.digivox.api.error.OutOptionsException;
import br.com.api.digivox.api.error.ResourceNotFoundException;
import br.com.api.digivox.api.repositories.ClienteRepository;

@Component
public class ClienteModel {
	@Autowired
	private ClienteRepository repository;
	@Autowired
	private AluguelModel aluguelModel;
	@Autowired
	private ReservaModel reservaModel;
	
	
	public Cliente save(Cliente cliente) {
		validaCliente(cliente);
		return repository.save(cliente);
	}
	
	public Cliente update(Cliente cliente) {
		validaCliente(cliente);

		return repository.save(cliente);
	}
	
	public void delete(long id) {
		Cliente cliente = retornaClienteValidoParaExclusao(id);
		
		repository.delete(cliente);
	}
	
	public List<Cliente> getAll() {
		return (List<Cliente>) repository.findAll();
	}
	
	public Cliente getById(long id) {
		return repository.findById(id).orElse(null);
	}
	
	public Cliente getByCpf(String cpf) {
		return repository.findByCpf(cpf);
	}
		
	public Cliente getByCnpj(String cnpj) {
		return repository.findByCnpj(cnpj);
	}
	
	private Cliente retornaClienteValidoParaExclusao(long id) {
		Cliente cliente = getById(id);
		if (cliente == null) {
			throw new ResourceNotFoundException("Erro ao remover, cliente não encontrado para o Id: " + id);
		}
		
		Aluguel aluguel = aluguelModel.getByCliente(cliente);
		if (aluguel != null) {
			throw new ApiException("Erro ao remover, cliente possui aluguel!");
		}
		
		Reserva reserva = reservaModel.getByCliente(cliente);
		if (reserva != null) {
			throw new ApiException("Erro ao remover, cliente possui reserva!");
		}
		
		return cliente;
	}
	
	private void validaCliente(Cliente cliente) {
		Cliente clienteConsulta = null;
		if(cliente.getTipo() != Cliente.TIPO_PESSOA_FISICA && cliente.getTipo() != Cliente.TIPO_PESSOA_JURIDICA) {
			throw new OutOptionsException("Erro ao gravar, tipo do cliente inválido! Valores aceitos: 1 - Pessoa Física; 2 - Pessoa Jurídica");
		}
		
		if(cliente.getTipo() == Cliente.TIPO_PESSOA_FISICA) {
			cliente.setCnpj(null);
			if(cliente.getCpf() == null) {
				throw new ApiException("Erro ao gravar, necessário informar o cpf!");				
			}
			
			clienteConsulta = getByCpf(cliente.getCpf());
		}
		
		if(cliente.getTipo() == Cliente.TIPO_PESSOA_JURIDICA) {
			cliente.setCpf(null);
			if(cliente.getCnpj() == null) {
				throw new ApiException("Erro ao gravar, necessário informar o cnpj!");				
			}
			
			clienteConsulta = getByCnpj(cliente.getCnpj());
		}
		
		if(cliente.getId() == 0) {
			if (clienteConsulta != null) {
				throw new DuplicatedKeyException("Erro ao gravar, já existe um cliente como esse Cnpf/Cnpj cadastrado!");
			}
			
		} else {
			Cliente clienteConsultaInterna = getById(cliente.getId());
			if (clienteConsultaInterna == null) {
				throw new ResourceNotFoundException("Erro ao atualizar, cliente não encontrado para o Id: " + cliente.getId());
			}
			
			if(clienteConsulta != null && clienteConsulta.getId() != cliente.getId()) {
				throw new DuplicatedKeyException("Erro ao atualizar, já existe um cliente como esse Cnpf/Cnpj cadastrado!");
			}
		}
	}
}
