package br.com.api.digivox.api.routes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.digivox.api.entity.Item;
import br.com.api.digivox.api.models.ItemModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@RestController
@RequestMapping(path = "itens")
public class ItemRoute {
	@Autowired
	private ItemModel itemModel;
	
	@ApiOperation(value = "Salva um item na base de dados", response = Item.class)
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Item item) {
		return new ResponseEntity<>(itemModel.save(item), HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Atualiza um item na base de dados", response = Item.class)
	@PutMapping
	public ResponseEntity<?> update(@Valid @RequestBody Item item) {
		return new ResponseEntity<>(itemModel.update(item), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Remove um item da base de dados")
	@DeleteMapping(path = "{id}")
	public ResponseEntity<?> delete(@PathVariable long id) {
		itemModel.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera os itens na base de dados", response = Item[].class)
	@GetMapping
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(itemModel.getAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera o item pelo id na base de dados", response = Item.class)
	@GetMapping(path = "{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		return new ResponseEntity<>(itemModel.getById(id), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retorna a quantidade de itens a serem devolvidos na semana(Data atual mais 7 dias)", response = Long.class)
	@GetMapping(path = "/qtde-itens-a-serem-devolvidos")
	public ResponseEntity<?> getQtdeDeItensASeremDevolvidosNaSemana() {
		return new ResponseEntity<>(itemModel.getQtdeDeItensASeremDevolvidosNaSemana(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retorna a quantidade de itens alugados na semana(Data atual mais 7 dias)", response = Long.class)
	@GetMapping(path = "/qtde-itens-alugados")
	public ResponseEntity<?> getQtdeDeItensAlugadosNaSemana() {
		return new ResponseEntity<>(itemModel.getQtdeDeItensAlugadosNaSemana(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retorna os itens a serem devolvidos na semana(Data atual mais 7 dias)", response = Item[].class)
	@GetMapping(path = "/itens-a-serem-devolvidos")
	public ResponseEntity<?> getItensASeremDevolvidosNaSemana() {
		return new ResponseEntity<>(itemModel.getItensASeremDevolvidosNaSemana(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retorna os itens alugados na semana(Data atual mais 7 dias)", response = Item[].class)
	@GetMapping(path = "/itens-alugados")
	public ResponseEntity<?> getItensAlugadosNaSemana() {
		return new ResponseEntity<>(itemModel.getItensAlugadosNaSemana(), HttpStatus.OK);
	}
}
