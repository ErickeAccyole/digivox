package br.com.api.digivox.api.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.api.digivox.api.entity.Aluguel;
import br.com.api.digivox.api.entity.Cliente;
import br.com.api.digivox.api.entity.Item;
import br.com.api.digivox.api.error.ApiException;
import br.com.api.digivox.api.error.ResourceNotFoundException;
import br.com.api.digivox.api.repositories.AluguelRepository;
import br.com.api.digivox.api.utils.Funcao;

@Component
public class AluguelModel {
	@Autowired
	private AluguelRepository repository;
	@Autowired
	private ItemModel itemModel;
	@Autowired
	private ClienteModel clienteModel;
	
	public Aluguel save(Aluguel aluguel) {
		aluguel.setData(Funcao.getDataHoraAtual());
		validaAluguel(aluguel);

		return repository.save(aluguel);
	}
	
	public Aluguel update(Aluguel aluguel) {
		return repository.save(aluguel);
	}
	
	public void delete(long id) {
		Aluguel aluguel = retornaAluguelValidoParaCancelamento(id);
		
		repository.delete(aluguel);
	}

	public void finalizarAluguel(long id) {
		Aluguel aluguel = getById(id);
		if (aluguel == null) {
			throw new ResourceNotFoundException("Erro ao finalizar, aluguel não encontrado! Id: " + id);
		}
		
		aluguel.setDataDevolucao(Funcao.getDataHoraAtual());
		update(aluguel);
	}
	
	public List<Aluguel> getAll() {
		return (List<Aluguel>) repository.findAll();
	}
	
	public Aluguel getById(long id) {
		return repository.findById(id).orElse(null);
	}
	
	public Aluguel getByCliente(Cliente cliente) {
		return repository.findFirstByCliente(cliente);
	}
	
	private Aluguel retornaAluguelValidoParaCancelamento(long id) {
		Aluguel aluguel = getById(id);
		if (aluguel == null) {
			throw new ResourceNotFoundException("Erro ao remover, aluguel não encontrado! Id:" + id);
		}
		
		return aluguel;
	}
	
	private void validaAluguel(Aluguel aluguel) {
		boolean houveErro = false;
		StringBuilder msgErro = null;
		List<Long> idsItens = null;
		
		if (aluguel.getDataExpiracao() == null) {
			throw new ApiException("Erro ao gravar aluguel, campo dataExpiracao é obrigatório!");
		}
		
		if (aluguel.getCliente() == null || aluguel.getCliente().getId() == 0) {
			throw new ApiException("Necessário informar algum cliente!");
		}
		
		if (aluguel.getItens() == null || aluguel.getItens().isEmpty()) {
			throw new ApiException("Necessário informar algum item!");
		}
		
		Cliente cliente = clienteModel.getById(aluguel.getCliente().getId()); // valida se o cliente existe na base de dados
		if (cliente == null) {
			throw new ResourceNotFoundException("Erro ao gravar, cliente da reserva não encontrado! Id:" + aluguel.getCliente().getId());
		}
		
		msgErro = new StringBuilder();
		msgErro.append("Erro ao gravar a reserva, Os seguintes itens não foram encontrados:");

		idsItens = new ArrayList<Long>();
		for (Item item : aluguel.getItens()) { // Valida se o item adicionado ao aluguel existe na base de dados
			Item itemConsulta = itemModel.getById(item.getId());
			if(itemConsulta == null) {
				msgErro.append("\n ID do item: ").append(item.getId());
				houveErro = true;
				continue;
			}

			idsItens.add(item.getId());
		}
		
		if(houveErro) {
			throw new ResourceNotFoundException(msgErro.toString());
		}

		// Verificando se algum dos itens ja estao alugados
		List<Item> itensAlugados = itemModel.getItensAlugadosPorData(aluguel.getData(), idsItens);
		if (!itensAlugados.isEmpty()) {
			msgErro = new StringBuilder();
			msgErro.append("Erro ao registrar o aluguel, Os seguintes itens já estão alugados:");
			
			for (Item item : itensAlugados) {
				msgErro.append("\n Item: ").append(item.getNome());
			}
			
			throw new ApiException(msgErro.toString());
		}
		
		// Verificando se algum dos itens ja estao reservados
		List<Item> itensReservados = itemModel.getItensReservadosPorData(aluguel.getData(), idsItens);
		if (!itensReservados.isEmpty()) {
			msgErro = new StringBuilder();
			msgErro.append("Erro ao registrar o aluguel, Os seguintes itens estão reservados:");
			
			for (Item item : itensReservados) {
				msgErro.append("\n Item: ").append(item.getNome());
			}
			
			throw new ApiException(msgErro.toString());
		}
	}
}
