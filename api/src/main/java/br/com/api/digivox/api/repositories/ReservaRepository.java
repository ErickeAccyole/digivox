package br.com.api.digivox.api.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.api.digivox.api.entity.Cliente;
import br.com.api.digivox.api.entity.Reserva;

public interface ReservaRepository extends CrudRepository<Reserva, Long>{

	Reserva findFirstByCliente(Cliente cliente);

}
