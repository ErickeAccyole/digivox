package br.com.api.digivox.api.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.api.digivox.api.entity.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

	Cliente findByCnpj(String cnpj);

	Cliente findByCpf(String cpf);

}
