package br.com.api.digivox.api.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class TipoItem extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	
	@Column(nullable = false, unique = true)
	@NotBlank
	private String nome;
	
	@OneToMany(mappedBy = "tipo")
	@JsonIgnore
	private List<Item> itens;
	
	
	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
