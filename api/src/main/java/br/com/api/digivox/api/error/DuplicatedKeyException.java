package br.com.api.digivox.api.error;

public class DuplicatedKeyException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public DuplicatedKeyException(String message) {
		super(message);
	}

}
