package br.com.api.digivox.api.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.api.digivox.api.entity.TipoItem;

public interface TipoItemRepository extends CrudRepository<TipoItem, Long> {

	TipoItem findByNome(String nome);

}
