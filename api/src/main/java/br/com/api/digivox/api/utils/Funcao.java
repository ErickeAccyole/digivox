package br.com.api.digivox.api.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.api.digivox.api.error.ApiException;

public class Funcao {
	public static Date converteDataSql(Date data) {
		try {
			String dataFormatada = new SimpleDateFormat("yyyy-MM-dd").format(data);
			return new SimpleDateFormat("yyyy-MM-dd").parse(dataFormatada);
		} catch (Exception e) {
			throw new ApiException("Erro ao formatar data!");
		}
	}
	
	public static Date getDataHoraAtual() {
		String dataFormatada = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dataFormatada);
		} catch (ParseException e) {
			throw new ApiException("Erro ao recuperar data atual!");
		}
	}
}
