package br.com.api.digivox.api.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Reserva extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm", timezone = "UTC-03:00") 
	private Date data;
	
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "UTC-03:00")
	@Column(name = "data_inicial", nullable = false)
	private Date dataInicial;
	
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "UTC-03:00")
	@Column(name = "data_final", nullable = false)
	private Date dataFinal;
	
	@ColumnDefault(value = "false")
	private boolean cancelada;
	
	@ManyToMany
	private List<Item> itens;
	
	@ManyToOne(optional = false)
	private Cliente cliente;
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public boolean isCancelada() {
		return cancelada;
	}

	public void setCancelada(boolean cancelada) {
		this.cancelada = cancelada;
	}
}
