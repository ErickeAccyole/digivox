package br.com.api.digivox.api.models;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.api.digivox.api.entity.Item;
import br.com.api.digivox.api.entity.TipoItem;
import br.com.api.digivox.api.error.ApiException;
import br.com.api.digivox.api.error.DuplicatedKeyException;
import br.com.api.digivox.api.error.ResourceNotFoundException;
import br.com.api.digivox.api.repositories.ItemRepository;
import br.com.api.digivox.api.utils.Funcao;

@Component
public class ItemModel {
	@Autowired
	private ItemRepository repository;
	@Autowired
	private TipoItemModel tipoItemModel;
	
	public Item save(Item item) {
		validaItem(item);
		
		return repository.save(item);
	}
	
	public Item update(Item item) {
		validaItem(item);
		
		return repository.save(item);
	}
	
	public void delete(long id) {
		Item item = retornaItemValidoParaExclusao(id);
		
		repository.delete(item);
	}
	
	public List<Item> getAll() {
		return (List<Item>) repository.findAll();
	}
	
	public Item getById(long id) {
		return repository.findById(id).orElse(null);
	}
	
	public Item getByCodigo(long codigo) {
		return repository.findByCodigo(codigo);
	}
	
	public List<Item> getItensAlugadosPorData(Date data, List<Long> idsItens) {
		return repository.getItensAlugadosPorData(Funcao.converteDataSql(data), idsItens);
	}
	
	public List<Item> getItensReservadosPorData(Date data, List<Long> idsItens) {
		return repository.getItensReservadosPorData(Funcao.converteDataSql(data), idsItens);
	}
	
	public List<Item> getItensReservadosPorDataInicialEFinal(Date dataInicial, Date dataFinal, List<Long> idsItens) {
		return repository.getItensReservadosPorDataInicialEFinal(Funcao.converteDataSql(dataInicial),Funcao.converteDataSql(dataFinal), idsItens);
	}
	
	public List<Item> getItensAlugadosPorDataInicialEFinal(Date dataInicial, Date dataFinal, List<Long> idsItens) {
		return repository.getItensAlugadosPorDataInicialEFinal(Funcao.converteDataSql(dataInicial),Funcao.converteDataSql(dataFinal), idsItens);
	}
	
	public long getQtdeDeItensASeremDevolvidosNaSemana() {
		return repository.itensASeremDevolvidosNaSemana().size();
	}
	
	public List<Item> getItensASeremDevolvidosNaSemana() {
		return repository.itensASeremDevolvidosNaSemana();
	}
	
	public long getQtdeDeItensAlugadosNaSemana() {
		return repository.itensAlugadosNaSemana().size();
	}
	
	public List<Item> getItensAlugadosNaSemana() {
		return repository.itensAlugadosNaSemana();
	}
	
	public Item getByTipoItem(TipoItem tipoItem) {
		return repository.findFirstByTipo(tipoItem);				
	}
	
	private Item retornaItemValidoParaExclusao(long id) {
		Item item = getById(id);
		if (item == null) {
			throw new ResourceNotFoundException("Erro ao remover, item não encontrado! Id:" + id);
		}
		
		if (!item.getAlugueis().isEmpty()) {
			throw new ApiException("Erro ao remover, o item pertence a algum aluguel!");
		}
		
		if (!item.getReservas().isEmpty()) {
			throw new ApiException("Erro ao remover, o item pertence a alguma reserva!");
		}

		return item;
	}
	
	private void validaItem(Item item) {
		if (item.getCodigo() == 0) {
			throw new ApiException("Necessário informar o codigo do item!");
		}
		
		Item itemConsulta = getByCodigo(item.getCodigo());
		
		if(item.getId() == 0) {
			if (itemConsulta != null) {
				throw new DuplicatedKeyException("Erro ao gravar, já existe um item como esse código cadastrado!");
			}
		} else {
			Item itemConsultaInterna = getById(item.getId());
			if (itemConsultaInterna == null) {
				throw new ResourceNotFoundException("Erro ao atualizar, item não encontrado para o Id: " + item.getId());
			}
			
			if(itemConsulta != null && itemConsulta.getId() != item.getId()) {
				throw new DuplicatedKeyException("Erro ao atualizar, já existe um item como esse código cadastrado!");
			}
		}

		if (item.getTipo() == null || item.getTipo().getId() == 0) {
			throw new ApiException("Necessário informar algum item!");
		}
		
		TipoItem tipo = tipoItemModel.getById(item.getTipo().getId());
		if (tipo == null) {
			throw new ResourceNotFoundException("Erro ao atualizar, tipo do item não encontrado para o Id: " + item.getTipo().getId());
		}
	}
}
