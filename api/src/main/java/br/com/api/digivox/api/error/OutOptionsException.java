package br.com.api.digivox.api.error;

public class OutOptionsException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public OutOptionsException(String message) {
		super(message);
	}
}
