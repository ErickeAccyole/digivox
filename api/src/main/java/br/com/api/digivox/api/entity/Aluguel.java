package br.com.api.digivox.api.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Aluguel extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm", timezone = "UTC-03:00")
	private Date data;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm", timezone = "UTC-03:00")
	@Column(name = "data_devolucao")
	private Date dataDevolucao;
	
	@JsonFormat(pattern = "dd/MM/yyyy", timezone = "UTC-03:00")
	@Column(name = "data_expiracao", nullable = false)
	private Date dataExpiracao;
	
	@ManyToMany
	private List<Item> itens;

	@ManyToOne(optional = false)
	private Cliente cliente;
	

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public Date getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataExpiracao(Date dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}
}
