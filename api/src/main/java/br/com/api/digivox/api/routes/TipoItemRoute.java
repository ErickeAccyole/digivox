package br.com.api.digivox.api.routes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.digivox.api.entity.TipoItem;
import br.com.api.digivox.api.models.TipoItemModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@RestController
@RequestMapping(path = "tipos-itens")
public class TipoItemRoute {
	@Autowired
	private TipoItemModel tipoItemModel;
	
	@ApiOperation(value = "Salva um tipo de item na base de dados", response = TipoItem.class)
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody TipoItem tipoItem) {
		return new ResponseEntity<>(tipoItemModel.save(tipoItem), HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Atualiza um tipo de item na base de dados", response = TipoItem.class)
	@PutMapping
	public ResponseEntity<?> update(@Valid @RequestBody TipoItem tipoItem) {
		return new ResponseEntity<>(tipoItemModel.update(tipoItem), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Remove um tipo de item na base de dados")
	@DeleteMapping(path = "{id}")
	public ResponseEntity<?> delete(@PathVariable long id) {
		tipoItemModel.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera os tipos de itens na base de dados", response = TipoItem[].class)
	@GetMapping
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(tipoItemModel.getAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera o item pelo id na base de dados", response = TipoItem.class)
	@GetMapping(path = "{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		return new ResponseEntity<>(tipoItemModel.getById(id), HttpStatus.OK);
	}
}
