package br.com.api.digivox.api.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.api.digivox.api.entity.Cliente;
import br.com.api.digivox.api.entity.Item;
import br.com.api.digivox.api.entity.Reserva;
import br.com.api.digivox.api.error.ApiException;
import br.com.api.digivox.api.error.ResourceNotFoundException;
import br.com.api.digivox.api.repositories.ReservaRepository;
import br.com.api.digivox.api.utils.Funcao;

@Component
public class ReservaModel {	
	@Autowired
	private ReservaRepository repository;
	@Autowired
	private ItemModel itemModel;
	@Autowired
	private ClienteModel clienteModel;
	
	public Reserva save(Reserva reserva) {
		reserva.setData(Funcao.getDataHoraAtual());
		validaReserva(reserva);	
		
		return repository.save(reserva);
	}
	
	public Reserva update(Reserva reserva) {
		return repository.save(reserva);
	}
	
	public void delete(long id) {
		Reserva reserva = getById(id);
		if (reserva == null) {
			throw new ResourceNotFoundException("Erro ao remover, reserva não encontrada!");
		}
		
		repository.delete(reserva);
	}
	
	public void cancelaReserva(long id) {
		Reserva reserva = retornaReservaValidaParaCancelamento(id); 
		
		reserva.setCancelada(true);
		update(reserva);
	}
	
	public List<Reserva> getAll() {
		return (List<Reserva>) repository.findAll();
	}
	
	public Reserva getById(long id) {
		return repository.findById(id).orElse(null);
	}

	public Reserva getByCliente(Cliente cliente) {
		return repository.findFirstByCliente(cliente);
	}
	
	private void validaReserva(Reserva reserva) {
		boolean houveErro = false;
		StringBuilder msgErro = null;
		List<Long> idsItens = null;
		
		if (reserva.getCliente() == null || reserva.getCliente().getId() == 0) {
			throw new ApiException("Necessário informar algum cliente!");
		}
		
		if (reserva.getItens() == null || reserva.getItens().isEmpty()) {
			throw new ApiException("Necessário informar algum item!");
		}
		
		Cliente cliente = clienteModel.getById(reserva.getCliente().getId()); // valida se o cliente existe na base de dados
		if (cliente == null) {
			throw new ResourceNotFoundException("Erro ao gravar, cliente da reserva não encontrado! Id:" + reserva.getCliente().getId());
		}
		
		msgErro = new StringBuilder();
		msgErro.append("Erro ao gravar a reserva, Os seguintes itens não foram encontrados:");
		
		idsItens = new ArrayList<Long>(); // Valida se o item adicionado a reserva existe na base de dados
		for (Item item : reserva.getItens()) {
			Item itemConsulta = itemModel.getById(item.getId());
			if(itemConsulta == null) {
				msgErro.append("\n ID do item: ").append(item.getId());
				houveErro = true;
				continue;
			}
			
			idsItens.add(item.getId());
		}
		
		if(houveErro) {
			throw new ResourceNotFoundException(msgErro.toString());
		}
		
		// Verificando se algum dos itens ja estao reservados
		List<Item> itensReservados = itemModel.getItensReservadosPorDataInicialEFinal(reserva.getDataInicial(), reserva.getDataFinal(), idsItens);
		if (!itensReservados.isEmpty()) {
			msgErro = new StringBuilder();
			msgErro.append("Erro ao gravar a reserva, Os seguintes itens estão reservados:");
			
			for (Item item : itensReservados) {
				msgErro.append("\n Item: ").append(item.getNome());
			}
			
			throw new ApiException(msgErro.toString());
		}
		
		// Verificando se algum dos itens ja estao alugados
		List<Item> itensAlugados = itemModel.getItensAlugadosPorDataInicialEFinal(reserva.getDataInicial(), reserva.getDataFinal(), idsItens);
		if (!itensAlugados.isEmpty()) {
			msgErro = new StringBuilder();
			msgErro.append("Erro ao gravar a reserva, Os seguintes itens já estão alugados:");

			for (Item item : itensAlugados) {
				msgErro.append("\n Item: ").append(item.getNome());
			}

			throw new ApiException(msgErro.toString());
		}
	}
	
	private Reserva retornaReservaValidaParaCancelamento(long id) {
		Reserva reserva = getById(id);
		if (reserva == null) {
			throw new ResourceNotFoundException("Erro ao cancelar, reserva não encontrada! Id:" + id);
		}
		
		return reserva;
	}
}
