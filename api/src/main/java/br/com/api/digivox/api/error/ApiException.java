package br.com.api.digivox.api.error;

public class ApiException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public ApiException(String message) {
		super(message);
	}
}
