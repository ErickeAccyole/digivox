package br.com.api.digivox.api.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.api.digivox.api.entity.Aluguel;
import br.com.api.digivox.api.entity.Cliente;

public interface AluguelRepository extends CrudRepository<Aluguel, Long>{
	
	Aluguel findFirstByCliente(Cliente cliente);

}
