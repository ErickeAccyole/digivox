package br.com.api.digivox.api.models;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.api.digivox.api.entity.Item;
import br.com.api.digivox.api.entity.TipoItem;
import br.com.api.digivox.api.error.ApiException;
import br.com.api.digivox.api.error.DuplicatedKeyException;
import br.com.api.digivox.api.error.ResourceNotFoundException;
import br.com.api.digivox.api.repositories.TipoItemRepository;

@Component
public class TipoItemModel {
	@Autowired
	private TipoItemRepository repository;
	@Autowired
	private ItemModel itemModel;
	
	public TipoItem save(TipoItem tipoItem) {
		validaTipoItem(tipoItem);
		
		return repository.save(tipoItem);
	}
	
	public TipoItem update(TipoItem tipoItem) {
		validaTipoItem(tipoItem);

		return repository.save(tipoItem);
	}
	
	public void delete(long id) {
		TipoItem tipoItem = retornaTipoItemValidoParaExclusao(id);
		
		repository.delete(tipoItem);
	}
	
	public List<TipoItem> getAll() {
		return (List<TipoItem>) repository.findAll();
	}
	
	public TipoItem getById(long id) {
		return repository.findById(id).orElse(null);
	}
	
	public TipoItem getByNome(String nome) {
		return repository.findByNome(nome);
	}
	
	private TipoItem retornaTipoItemValidoParaExclusao(long id) {
		TipoItem tipoItem = getById(id);
		if (tipoItem == null) {
			throw new ResourceNotFoundException("Erro ao remover, tipo do item não encontrado para o Id: " + id);
		}
		
		Item item = itemModel.getByTipoItem(tipoItem);
		if (item != null) {
			throw new ApiException("Erro ao remover, existe algum item com esse tipo!");
		}
		
		return tipoItem;
	}
	
	private void validaTipoItem(TipoItem tipoItem) {
		TipoItem tipoConsulta = getByNome(tipoItem.getNome());
		
		if(tipoItem.getId() == 0) { // Salvando
			if (tipoConsulta != null) {
				throw new DuplicatedKeyException("Erroa ao gravar, ja existe um tipo de item cadastrado com esse nome!");
			}
		} else { // Atualizando
			TipoItem tipoConsultaInterna = getById(tipoItem.getId());
			if (tipoConsultaInterna == null) {
				throw new ResourceNotFoundException("Erro ao remover, tipo do item não encontrado para o Id: " + tipoItem.getId());
			}
			
			if(tipoConsulta != null && tipoConsulta.getId() != tipoItem.getId()) {
				throw new DuplicatedKeyException("Erro ao atualizar, já existe um tipo de item como esse nome cadastrado!");
			}
		}
	}
}
