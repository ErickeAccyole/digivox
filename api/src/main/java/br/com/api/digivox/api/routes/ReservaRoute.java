package br.com.api.digivox.api.routes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.digivox.api.entity.Reserva;
import br.com.api.digivox.api.models.ReservaModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api
@RestController
@RequestMapping(path = "reservas")
public class ReservaRoute {
	@Autowired
	private ReservaModel reservaModel;
	
	@ApiOperation(value = "Salva uma reserva na base de dados", response = Reserva.class)
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Reserva reserva) {
		return new ResponseEntity<>(reservaModel.save(reserva), HttpStatus.CREATED);
	}
	
	@ApiIgnore
	@PutMapping
	public ResponseEntity<?> update(@Valid @RequestBody Reserva reserva) {
		return new ResponseEntity<>(reservaModel.update(reserva), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Cancela uma reserva")
	@PutMapping(path = "{id}")
	public ResponseEntity<?> cancelaReserva(@PathVariable long id) {
		reservaModel.cancelaReserva(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera os reservas na base de dados", response = Reserva[].class)
	@GetMapping
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(reservaModel.getAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera a reserva pelo id na base de dados", response = Reserva.class)
	@GetMapping(path = "{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		return new ResponseEntity<>(reservaModel.getById(id), HttpStatus.OK);
	}
}
