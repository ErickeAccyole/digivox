package br.com.api.digivox.api.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Cliente extends AbstractEntity {
	private static final long serialVersionUID = 1L;
	public static final int TIPO_PESSOA_FISICA = 1;
	public static final int TIPO_PESSOA_JURIDICA = 2;
	
	@Column(nullable = false)
	@NotBlank
	private String nome;
	
	@Column(nullable = false)
	private int tipo;
	
	@Column(unique = true)
	@CPF
	private String cpf;
	
	@Column(unique = true)
	@CNPJ
	private String cnpj;
	
	@JsonIgnore
	@OneToMany(mappedBy = "cliente")
	private List<Aluguel> alugueis;
	
	@JsonIgnore
	@OneToMany(mappedBy = "cliente")
	private List<Reserva> reservas;
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Aluguel> getAlugueis() {
		return alugueis;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public void setAlugueis(List<Aluguel> alugueis) {
		this.alugueis = alugueis;
	}

	public List<Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(List<Reserva> reservas) {
		this.reservas = reservas;
	}
}
