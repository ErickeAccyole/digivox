package br.com.api.digivox.api.routes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.digivox.api.entity.Cliente;
import br.com.api.digivox.api.models.ClienteModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@RestController
@RequestMapping(path = "clientes")
public class ClienteRoute {
	@Autowired
	private ClienteModel clienteModel;
	
	@ApiOperation(value = "Salva um cliente na base de dados", response = Cliente.class)
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Cliente cliente) {
		return new ResponseEntity<>(clienteModel.save(cliente), HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Atualiza um cliente na base de dados", response = Cliente.class)
	@PutMapping
	public ResponseEntity<?> update(@Valid @RequestBody Cliente cliente) {
		return new ResponseEntity<>(clienteModel.update(cliente), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Remove um cliente da base de dados")
	@DeleteMapping(path = "{id}")
	public ResponseEntity<?> delete(@PathVariable long id) {
		clienteModel.delete(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera os clientes na base de dados", response = Cliente[].class)
	@GetMapping
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(clienteModel.getAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera o cliente pelo id na base de dados", response = Cliente.class)
	@GetMapping(path = "{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		return new ResponseEntity<>(clienteModel.getById(id), HttpStatus.OK);
	}
}
