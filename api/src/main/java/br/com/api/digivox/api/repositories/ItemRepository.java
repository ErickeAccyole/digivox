package br.com.api.digivox.api.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.api.digivox.api.entity.Item;
import br.com.api.digivox.api.entity.TipoItem;

public interface ItemRepository extends CrudRepository<Item, Long> {
	@Query(
		value = "SELECT i " + 
		"FROM Item i " + 
		"JOIN i.alugueis a " + 
		"WHERE date(a.dataExpiracao) BETWEEN current_date AND current_date + 7" + 
		"  AND a.dataDevolucao IS NULL")
	List<Item> itensASeremDevolvidosNaSemana();
	
	@Query(
		value = "SELECT i " + 
		"FROM Item i " + 
		"JOIN i.alugueis a " + 
		"WHERE date(a.data) BETWEEN current_date AND current_date + 7")
	List<Item> itensAlugadosNaSemana();
	
	@Query(
		value = "SELECT i " + 
		"FROM Item i " + 
		"JOIN i.alugueis a " + 
		"WHERE i.id IN :ids " + 
		"  AND :data BETWEEN date(a.data) AND date(a.dataExpiracao)" + 
		"  AND a.dataDevolucao IS NULL")
	List<Item> getItensAlugadosPorData(@Param("data") Date data, @Param("ids") List<Long> idsItens);
	
	@Query(
		value = "SELECT i " + 
				"FROM Item i " + 
				"JOIN i.reservas r " + 
				"WHERE i.id IN :ids " + 
				"  AND :data BETWEEN date(r.dataInicial) AND date(r.dataFinal)" + 
				"  AND r.cancelada = false")
	List<Item> getItensReservadosPorData(@Param("data") Date data, @Param("ids") List<Long> idsItens);
	
	@Query(
		value = "SELECT i " + 
				"FROM Item i " +
				"JOIN i.reservas r " +
				"WHERE (" +
				"			:dataInicial BETWEEN date(r.dataInicial) AND (r.dataFinal) " + 
				"       OR " +
				"			:dataFinal BETWEEN date(r.dataInicial) AND (r.dataFinal)" +
				"	   )" +
				"  AND r.cancelada = false" +
				"  AND i.id IN :ids ")
	List<Item> getItensReservadosPorDataInicialEFinal(@Param("dataInicial") Date dataInicial, @Param("dataFinal") Date dataFinal, @Param("ids") List<Long> idsItens);
	
	@Query(
		value = "SELECT i " + 
				"FROM Item i " +
				"JOIN i.alugueis a " +
				"WHERE (" +
				"			:dataInicial BETWEEN date(a.data) AND (a.dataExpiracao) " + 
				"       OR " +
				"			:dataFinal BETWEEN date(a.data) AND (a.dataExpiracao)" +
				"	   )" +
				"  AND a.dataDevolucao IS NULL" +
				"  AND i.id IN :ids ")
	List<Item> getItensAlugadosPorDataInicialEFinal(@Param("dataInicial") Date dataInicial, @Param("dataFinal") Date dataFinal, @Param("ids") List<Long> idsItens);

	Item findByCodigo(long codigo);
	
	Item findFirstByTipo(TipoItem tipoItem);
}
