package br.com.api.digivox.api.routes;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.digivox.api.entity.Aluguel;
import br.com.api.digivox.api.models.AluguelModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api
@RestController
@RequestMapping(path = "alugueis")
public class AluguelRoute {
	@Autowired
	private AluguelModel aluguelModel;
	
	@ApiOperation(value = "Salva um aluguel na base de dados", response = Aluguel.class)
	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Aluguel aluguel) {
		return new ResponseEntity<>(aluguelModel.save(aluguel), HttpStatus.CREATED);
	}
	
	@ApiOperation(value = "Finaliza um aluguel")
	@PutMapping(path = "{id}")
	public ResponseEntity<?> finalizarAluguel(@PathVariable long id) {
		aluguelModel.finalizarAluguel(id);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera os alugueis na base de dados", response = Aluguel[].class)
	@GetMapping
	public ResponseEntity<?> getAll() {
		return new ResponseEntity<>(aluguelModel.getAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Recupera o aluguel pelo id na base de dados", response = Aluguel.class)
	@GetMapping(path = "{id}")
	public ResponseEntity<?> getById(@PathVariable long id) {
		return new ResponseEntity<>(aluguelModel.getById(id), HttpStatus.OK);
	}
}
