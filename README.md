# API

O arquivo Desafio DigiVox.postman_collection contém todas as requisições para realizar o desafio como solicitado.

A aplicação também disponibiliza a documentação da api usando o Swagger. Segue o link:
	
	http://localhost:8080/api/v1/swagger-ui.html#/

Antes de subir a aplicação deve ser criado o banco de dados com de acordo com o arquivo application.properties:

	spring.datasource.url= jdbc:postgresql://localhost:5432/digivox
	spring.datasource.username= postgres
	spring.datasource.password= postgres




